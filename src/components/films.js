// src/components/films.js

import React from 'react';
import Filter from './filter';
import Pagination from "./pagination";
import Sorter from "./sorter";

const Films = ({films}) => {
    return (
        <div>
            <center><h1>Films List</h1></center>
            <Filter></Filter>
            <table className={'table'}>
                <thead>
                <tr>
                    <th onClick={Sorter}>tconst</th>
                    <th>titleType</th>
                    <th>primaryTitle</th>
                    <th>originalTitle</th>
                    <th>isAdult</th>
                    <th>startYear</th>
                    <th>endYear</th>
                    <th>runtimeMinutes</th>
                    <th>genres</th>
                    <th>averageRating</th>
                    <th>numVotes</th>
                </tr>
                </thead>
                <tbody>
                {films.map((film) => (
                    <tr>
                        <td>{film.tconst}</td>
                        <td>{film.titleType}</td>
                        <td>{film.primaryTitle}</td>
                        <td>{film.originalTitle}</td>
                        <td>{film.isAdult}</td>
                        <td>{film.startYear}</td>
                        <td>{film.endYear}</td>
                        <td>{film.runtimeMinutes}</td>
                        <td>{film.genres}</td>
                        <td>{film.averageRating}</td>
                        <td>{film.numVotes}</td>
                    </tr>
                ))}
                </tbody>
                <tfoot>
                <tr>
                    <td colSpan={9}><Pagination></Pagination></td>
                </tr>
                </tfoot>
            </table>
        </div>
    )
};

export default Films