// src/components/filter.js

import React from 'react'

const Filter = ({films}) => {
    return (
        <div>
            <div>
                <label>Rate</label>
                <input type="integer" name="rate"/>
            </div>
            <div>
                <label>Name</label>
                <input type="text" name="name"/>
            </div>
        </div>
    )
};

export default Filter