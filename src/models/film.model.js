const sql = require("./db.js");

const Rating = require("./rating.model");

// constructor
const Film = function (film) {
    this.tconst = film.tconst;
    this.titleType = film.titleType;
    this.primaryTitle = film.primaryTitle;
    this.originalTitle = film.originalTitle;
    this.isAdult = film.isAdult;
    this.startYear = film.startYear;
    this.endYear = film.endYear;
    this.runtimeMinutes = film.runtimeMinutes;
    this.genres = film.genres;
    this.averageRating = film.averageRating;
    this.numVotes = film.numVotes;
};

Film.getAll = result => {
    sql.query("SELECT * FROM films LEFT JOIN rating on films.tconst = rating.tconst", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        console.log("films: ", res);
        result(null, res);
    });
};

Film.findById = (tconst, result) => {
    sql.query(`SELECT *
               FROM films
               WHERE tconst = ${tconst}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found film: ", res[0]);
            result(null, res[0]);
            return;
        }

        // not found Film with the id
        result({kind: "not_found"}, null);
    });
};

module.exports = Film;