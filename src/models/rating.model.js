const sql = require("./db.js");

// constructor
const Rating = function (rate) {
    this.tconst = rate.tconst;
    this.averageRating = rate.averageRating;
    this.numVotes = rate.numVotes;
};

Rating.findById = (tconst, result) => {
    sql.query(`SELECT *
               FROM rating
               WHERE tconst = ${tconst}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found rate: ", res[0]);
            result(null, res[0]);
            return;
        }

        // not found Rating with the id
        result({kind: "not_found"}, null);
    });
};

module.exports = Rating;