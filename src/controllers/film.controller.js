const Film = require("../models/film.model");

// Retrieve all Films from the database.
exports.findAll = (req, res) => {
    Film.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving films."
            });
        else res.send(data);
    });
};

// Find a single Film with a tconst
exports.findOne = (req, res) => {
    Film.findById(req.params.tconst, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Film with id ${req.params.tconst}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving Film with id " + req.params.tconst
                });
            }
        } else res.send(data);
    });
};