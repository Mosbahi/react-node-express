module.exports = app => {
    const films = require("../controllers/film.controller");

    // Retrieve all Films
    app.get("/films", films.findAll);

    // Retrieve a single Film with customerId
    app.get("/films/:tconst", films.findOne);
};