import React, {Component} from 'react';
import Films from './components/films';

class App extends Component {
    state = {
        films: []
    }

    componentDidMount() {
        fetch('http://localhost:3005/films')
            .then(res => res.json())
            .then((data) => {
                this.setState({films: data})
            })
            .catch(console.log)
    }

    render() {
        return (
            <Films films={this.state.films}></Films>
        );
    }
}

export default App;
