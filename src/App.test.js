import {render, screen} from '@testing-library/react';
import App from './App';

test('Check the presence of films list', () => {
    render(<App/>);
    const linkElement = screen.getByText(/Films/i);
    expect(linkElement).toBeInTheDocument();
});
